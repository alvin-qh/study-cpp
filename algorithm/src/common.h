#ifndef __COMMON_H
#define __COMMON_H

typedef bool Bool;
typedef int Int;
typedef unsigned int UInt;
typedef void Void;

#define null nullptr

#endif // __COMMON_H
